This widget behaves just like core's entity reference autocomplete and entity 
reference tagging widgets, but instead of just displaying entity label, other 
fields can be chosen to be displayed.


Installation:

Add some entity reference field. Choose `Rendered Autocomplete` or 
`Rendered Autocomplete Tag style` and configure it to your liking.
